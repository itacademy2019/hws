﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HW41
{
    class Program
    {
        static void Main(string[] args)
        {

            // 5 баллов
            int k = 1;
            int step = 1000;

            for (int i = 0; i < 100; i++)
            {
                int start = k;
                int end = k + step;
                string path = @"C:\temp\" + $"{start}-{end-1}.txt";
                WritePrimes(path, start, end);
                k += step;
            }

            Console.ReadLine();
        }

        // 5 баллов
        /// <summary>
        /// Пишет в файл все простые числа между двумя числами
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        /// <param name="start">Начальное число</param>
        /// <param name="end">Конечное число</param>
        static void WritePrimes(string path, int start, int end)
        {
            var primes = GetPrimes(start, end);
            var text = string.Join(", ", primes);

            using (var sw = new StreamWriter(path, true, Encoding.UTF8))
            {
                sw.WriteLine(text);
            }
        }

        // 5 баллов
        /// <summary>
        /// Возвращает все простые числа в отрезке между двумя числами
        /// </summary>
        /// <param name="start">Начальное число</param>
        /// <param name="end">Конечное число</param>
        /// <returns></returns>
        static List<int> GetPrimes(int start, int end)
        {
            var result = new List<int>();
            for (int i = start; i < end; i++)
            {
                if(IsPrime(i))
                    result.Add(i);
            }

            return result;
        }


        // 10 баллов
        /// <summary>
        /// Проверка числа на простоту. Если число делится на числе между 2 и n-1, то число не простое
        /// </summary>
        /// <param name="n">Число, которое проверяем на четность</param>
        /// <returns></returns>
        static bool IsPrime(int n)
        {
            if (n < 2)
                return false;

            for (int k = 2; k < n; k++)
            {
                if (n % k == 0)
                    return false;
            }

            return true;
        }
    }
}
